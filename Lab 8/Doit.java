import java.util.*;
import java.io.*;
					//create a file first of txt 
public class Doit
{  
	public static void main(String[]args) throws FileNotFoundException        
	{ 
		Scanner input1 = new Scanner(System.in);
           
		InputStream input = null;					//Creating refrence of inputstream class
		OutputStream output = null;					//Creating refrence of outputstream class

		String filename;							//Creating variable for storing file name entered by user
		int words = 0, lines = 0, chars = 0;			//Initialising varialbes to zero for counting words, char and lines

		System.out.println("Enter File to be read: ");		//Allwoing user to enter file name
		filename = input1.nextLine();					//Storint "file name" in filename variale
		
		File file = new File(filename+".txt");			//Creating a object of file
		Scanner in = new Scanner(file);

		try									//Creating a try block for counting char in a file
		{	FileReader fr = new FileReader(file);		//Creating object fr of Filereader class which allow us to read from file
		
			while (fr.read() > -1)					//Condition for reading a char form file 
				chars++;						
		}
		catch(IOException e)						//Creating catch block after try block
		{	System.out.println("\nError Occured..");	}
		
		File filew = new File(filename+".txt");			//Creating another object of class file
		Scanner inw = new Scanner(filew);
		
		while(inw.hasNext())						//Reading a word from file
		{	inw.next();							//Going to next word in file
			words++;				
		}
		
		while(in.hasNextLine())						//Reading a line from file
		{	in.nextLine();						//Going to next line in file
			lines++;
		}

		System.out.println("\nNumber of characters: " + chars);	//Printing number of char in file
		System.out.println("\nNumber of words: " + words);		//Printing number of words in file
		System.out.println("\nNumber of lines: " + lines);		//Printing number of lines in file

		System.out.println("\n\nCopying the content of file "+ filename+" to new file called newfile.txt");
		
		File file1 = new File("newfile.txt");			//Creating another object of class file

		try									//Creating try block for copying file to another file
		{	input = new FileInputStream(file);			//Creating an object of class fileinputstream
			output = new FileOutputStream(file1);		//Creating an object of class fileoutputstream

			byte[] b = new byte[1024];
			int bytesRead;

			while ((bytesRead = input.read(b)) > 0)		//reading each bytes form old file
				output.write(b, 0, bytesRead);		//writing each byte which is copied to new file
	
			System.out.println("\nCopied to newfile.txt");
		}
		catch(IOException e)
		{	System.out.println("\nError Occured..");	}
		
		System.out.println("\n\nRenaming newfile.txt to renamedfile.txt");

		File file2 = new File("renamedfile.txt");			//Creating another object of class file

		if(file1.renameTo(file2))					//Replacing the name of oldfile with newfile
         		System.out.println("\nRenamed..");
      	else
	 		System.out.println("\nCannot rename...");
      
	}
}