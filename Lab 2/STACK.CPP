//***************************************************************
//    	HEADER FILES
//****************************************************************                       Run in Code::Blocks or Dec-C++

#include<iostream>
#include<stdlib.h>
#include<conio.h>
																						//last in first out
					      																		  
using namespace std;

//***************************************************************
//    	CLASS FOR STACK
//****************************************************************

class Stack
{
	private:
		int *st;														// Pointer to int
		int n, top;														//n for size of stack
		
	public:
		Stack();														//Constructors
		
		~Stack();														//Destructor...

		void size();													//Member Functions...	
		void push(int);
		void pop();
		void dis();

}s;

Stack::Stack()
{
    st = '\0';
    n = 0;
    top = -1;
}

Stack::~Stack()
{
    delete st;
    
}

//***************************************************************
//    	FUNCTION FOR SIZE NEEDED FOR STACK
//****************************************************************

void Stack::size()
{	cout<<"\n\n\tEnter Size Of Stack:";
	cin>>n;
	st = new int[n];														// Allocate n ints and save ptr in a
}
					      																		  
//***************************************************************
//    	FUNCTION FOR PUSHING A ELEMENT IN THE STACK
//****************************************************************

void Stack::push(int x)
{	if(top==n-1)
		cout<<"\n\n\t\t overflow";
	else
	{	top++;
		st[top]=x;
		cout<<"\n\n\t\t element pushed:";
	}
	getch();
}

//***************************************************************
//    	FUNCTION FOR POPING A ELEMENT FROM THE STACK
//****************************************************************

void Stack::pop()
{   if(top==-1)
		cout<<"\n\n\t\t underflow:";
	else
	{	int i=st[top];
		top--;
		cout<<"\n\n\t\t element poped:";
	}
	getch();
}

//***************************************************************
//    	FUNCTION FOR DISPLAYING THE ELEMENTS FROM THE STACK
//****************************************************************

void Stack::dis()
{   if(top==-1)
		cout<<"\n\n\t\t Empty:";
	else
	{	cout<<"\n\n";
		for(int i=top;i> -1 ;i--)
		{	cout<<"\t\t | "<<st[i]<<" |\n";
			cout<<"\t\t -----\n";
		}
		cout<<"\n\t\t top= "<<top;
	}
	getch();
}

//***************************************************************
//    	INTRODUCTION FUNCTION
//****************************************************************

void intro()
{
    system("cls");
    cout<<"\n\n\t\t         MADE BY : SHLOAK AGARWAL";
    cout<<"\n\n\t\t             ROLL NO : 1401105";
    cout<<"\n\n\t\t             enter to continue";
    getch();
}

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

int main()
{	intro();
	int ch;
    do
	{   system("cls");
		cout<<"\n\n\tMAIN MENU";
		cout<<"\n\n\t 1. Size Of Stack";
		cout<<"\n\n\t 2. Insert";
		cout<<"\n\n\t 3. Delet";
		cout<<"\n\n\t 4. Display";
		cout<<"\n\n\t 5. Exit";
		cout<<"\n\n\tPlease Select Your Option (1-5) ";
		cin>>ch;
		switch(ch)
		{	case 1:
				s.size();
				break;
			case 2:
				int x;
				cout<<"\n Enter element:";
				cin>>x;
				s.push(x);
				break;
			case 3:
				s.pop();
				break;
			case 4:
				s.dis();
				break;
			case 5:
				exit(1.2);
		}
	}while(ch!='5');
	return 0;
}

//***************************************************************
//    			END OF PROGRAM
//***************************************************************
