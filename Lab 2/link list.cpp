//***************************************************************
//    	HEADER FILES
//****************************************************************                       Run in Code::Blocks or Dec-C++

#include<iostream>
#include<conio.h>
#include<string.h>
#include<stdlib.h>

using namespace std;

void main_menu();											//FUNCTION DECLARATION

//***************************************************************
//    	  STRUCTURE FOR CUSTOMER
//****************************************************************

struct Customer
{   char name[20];
	long int no;

    Customer *cnext;
	
} *cfirst, *clast, *c;

//***************************************************************
//    	   STRUCTURE FOR PRODUCT
//****************************************************************

struct Product
{   int price, prono;
    char proname[20];

  	Product *pnext;

} *pfirst, *plast, *p, *p1;

//***************************************************************
//    	FUNCTION FOR CREATING A NEW CUSTOMER ACCOUNT
//****************************************************************

void add_cus(Customer *&cfirst, Customer *&clast)
{   system("cls");
	char ch;
	do
	{
		c=new Customer;

		cout<<"\n\t Enter Customer Name:";
    	cin>>c->name;

    	cout<<"\n\t Enter Customer Phone Number:";
    	cin>>c->no;

	    if(cfirst == '\0')
	    { 	cfirst=c;
	    	clast=c;
		}
	    else
	    {  	clast->cnext=c;	
			clast=clast->cnext;
		}
		clast->cnext='\0';

		cout<<"\nDo You Want To Add Another Product ? (y/n)";
        cin>>ch;
	    cout<<"\n";
	}while( ch == 'y' || ch == 'Y' );
}

//***************************************************************
//    	FUNCTION FOR SEARCHING A CUSTOMER ACCOUNT
//****************************************************************

void search_cus(Customer *cfirst, int ph)
{	int i=0;
	for(c=cfirst;c!='\0';c=c->cnext)
		if(c->no == ph)
		{	system("cls");
			cout<<"\n\t Customer Name:"<<c->name;
	    	cout<<"\n\t Customer Phone Number:"<<c->no;
	    	getch();
	    	i=1;
	    	break;
		}
	if(i==0)
	{	cout<<"\n\t SORRY NO SUCH CUSTOMER EXIST";
		getch();
	}
}

//***************************************************************
//    	FUNCTION OF CUSTOMER DETAIL
//****************************************************************

void cus_detail()
{   long int ph;
    char ch;
    do
	{   system("cls");
		cout<<"\n\n\n\tMENU";
		if(cfirst=='\0')
            cout<<"\n\n\n\n************  PLEASE ADD CUSTOMER FIRST  ************\n\n\n\n";
		cout<<"\n\n\t1. ADD CUSTOMER";
		cout<<"\n\n\t2. SEARCH CUSTOMER";
		cout<<"\n\n\t3. RETURN TO MAIN MENU";
		cout<<"\n\n\tPlease Select Your Option (1-3) ";
		cin>>ch;
		switch(ch)
		{
			case '1':
                add_cus(cfirst,clast);
				break;
			case '2':
				system("cls");
			    cout<<"\n\tEnter Customer Phone Number: ";
                cin>>ph;
				search_cus(cfirst, ph);
			   	break;
			case '3':
               main_menu();
           default:
			    cus_detail();
		}
	}while(ch!='3');

}

//***************************************************************
//    	FUNCTION FOR CREATING A NEW PRODUCT
//****************************************************************

void add_pro(Product *&pfirst, Product *&plast)
{   system("cls");
	char ch;
	do
	{
		p=new Product;

		cout<<"\n\t Enter Product Number:";
    	cin>>p->prono;

    	cout<<"\n\t Enter Product Name:";
    	cin>>p->proname;

    	cout<<"\n\t Enter Product Price:";
    	cin>>p->price;

	    if(pfirst == '\0')
	    { 	pfirst=p;
	    	plast=p;
		}
	    else
	    {  	plast->pnext=p;	
			plast=plast->pnext;
		}
		plast->pnext='\0';
		
		cout<<"\nDo You Want To Add Another Product ? (y/n)";
        cin>>ch;
	    cout<<"\n";
	}while( ch == 'y' || ch == 'Y' );
}

//***************************************************************
//    	FUNCTION FOR SHOWING ALL PRODUCTS
//****************************************************************

void show_pro(Product *pfirst)
{   system("cls");
	int i=1;
    cout<<"\n\n--------------------------------------------------------------------------------";
	cout<<"\n\n Sr No.\t Pr No.\t\t Pr Name\t\tPrice\n";
	cout<<"\n--------------------------------------------------------------------------------";
	for(p=pfirst;p!='\0';p=p->pnext)
    {	cout<<"\n "<<i<<"\t "<<p->prono<<"\t\t "<<p->proname<<"\t\t "<<p->price;
    	i++;
	}
    cout<<"\n\n\t\tenter to continue";
    getch();
}

//***************************************************************
//    	FUNCTION OF PRODUCT DETAIL
//****************************************************************

void pro_detail()
{   int no;
    char ch;
    do
	{   system("cls");
		cout<<"\n\n\n\tMENU";
		if(pfirst=='\0')
            cout<<"\n\n\n\n************  PLEASE ADD PRODUCT FIRST  ************\n\n\n\n";
		cout<<"\n\n\t1. ADD PRODUCT";
		cout<<"\n\n\t2. SHOW ALL PRODUCTS";
		cout<<"\n\n\t3. RETURN TO MAIN MENU";
		cout<<"\n\n\tPlease Select Your Option (1-3) ";
		cin>>ch;
		switch(ch)
		{
			case '1':
                add_pro(pfirst,plast);
				break;
			case '2':
				show_pro(pfirst);
				break;
            case '3':
               main_menu();
            default:
			    cus_detail();
		}
	}while(ch!='3');

}

//***************************************************************
//  FUNCTION TO PLACE ORDER AND GENERATING BILL FOR PRODUCTS
//****************************************************************

void bill()
{   system("cls");
    int  total=0,qty=0,amt,order_arr[50],quan[50],c=0,x;
	char ch;
	show_pro(pfirst);
	cout<<"\n................................................................................";
	cout<<"\n\t\t\t\tPLACE YOUR ORDER";
	cout<<"\n................................................................................\n";
	do{     cout<<"\n\nEnter The Product No. Of The Product : ";
            cin>>order_arr[c];
            cout<<"\nQuantity in number : ";
            cin>>quan[c];
            c++;
            cout<<"\nDo You Want To Order Another Product ? (y/n)";
            ch = getche();
            cout<<endl;
	}while( ch == 'y' || ch == 'Y' );
	cout<<"\n\nThank You For Placing The Order";
	getch();
	system("cls");
	cout<<"\n\t\t\t  GLORIOUS ELECTRONICS";
	cout<<"\n\t\t\t\tCASH MEMO";
	cout<<"\n\n--------------------------------------------------------------------------------";
	cout<<"\n\n Sr No.\t Pr No.\t Pr Name\t\tQty \tPrice \t Amount\n";
	cout<<"\n--------------------------------------------------------------------------------";
	for(x=0;x<c;x++)
		for(p1=pfirst;p1!='\0';p1=p1->pnext)
           if(p1->prono==order_arr[x])
            {
				amt=(p1->price)*(quan[x]);
				cout<<"\n "<<(x+1)<<"\t "<<order_arr[x]<<"\t "<<p1->proname<<"\t\t "<<quan[x]<<"\t "<<p1->price<<"\t "<<amt;
				total+=amt;
				qty+=quan[x];
			 }
	cout<<"\n\n--------------------------------------------------------------------------------";
	cout<<"\t\tQUANTITY = "<<qty<<"\t\tTOTAL = "<<total;
	cout<<"\n\n--------------------------------------------------------------------------------\n\n";
}

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

void main_menu()
{   char ch;
    do
	{   system("cls");
        cout<<"\n\n\n\tMAIN MENU";
		cout<<"\n\n\t1. CUSTOMER DETAILS";
		cout<<"\n\n\t2. PRODUCT DETAILS";
		cout<<"\n\n\t3. GENERATE BILL";
        cout<<"\n\n\t4. EXIT";
		cout<<"\n\n\tPlease Select Your Option (1-4) ";
		cin>>ch;
		switch(ch)
		{
			case '1':
				cus_detail();
				main_menu();
			case '2':
			    pro_detail();
				main_menu();
			case '3':
			    bill();
			case '4':
                exit(0);
		}
	}while(ch!='4');
}

//***************************************************************
//    	INTRODUCTION FUNCTION
//****************************************************************

void intro()
{
    system("cls");
    cout<<"\n\t\t\t   GLORIOUS ELECTRONICS\n";
    cout<<"\n\n\t\t         MADE BY : SHLOAK AGARWAL";
    cout<<"\n\n\t\t             ROLL NO : 1401105";
    cout<<"\n\n\t\t             enter to continue";
    getch();
}

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

int main()
{
    cfirst=clast='\0';
 	pfirst=plast='\0';
	intro();
	main_menu();

	return 0;
	
}

//***************************************************************
//    			END OF PROGRAM
//***************************************************************
