		Steps to install java environment on linux/ubuntu

-Uncompress  tar -xvf jdk-7u2-linux-x64.tar.gz (64bit).
-JDK 7 package is extracted into ./jdk1.7.0_02 directory. - Now move the JDK 7 directory to /usr/lib.
-sudo mv ./jdk1.7.0_02 /usr/lib/jvm/jdk1.7.0
-update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.7.0/bin/java" 1
-sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.7.0/bin/javac" 1
-sudo update-alternatives --install "/usr/bin/javaws" "javaws" 
 "/usr/lib/jvm/jdk1.7.0/bin/javaws" 1
-sudo update-alternatives --config java
-output:
	$sudo update-alternatives --config java
-There are 3 choices for the alternative java (providing /usr/bin/java).
-Selection Path Priority Status:
	* 0 /usr/lib/jvm/java-6-openjdk/jre/bin/java 1061 auto mode
	1 /usr/lib/jvm/java-6-openjdk/jre/bin/java 1061 manual mode
	2 /usr/lib/jvm/java-6-sun/jre/bin/java 63 manual mode
	3 /usr/lib/jvm/jdk1.7.0/jre/bin/java 3 manual mode
-Press enter to keep the current choice[*], or type selection number: 3
-update-alternatives: using /usr/lib/jvm/jdk1.7.0/jre/bin/java to provide /usr/bin/java (java) in manual mode.
-Check the version of you new JDK 7 installation:
	  java -version
	  java version �1.7.0�
	  Java(TM) SE Runtime Environment (build 1.7.0-b147)
	  Java HotSpot(TM) Client VM (build 21.0-b17, mixed mode) 
-Repeat the above for:
	sudo update-alternatives --config javac
	sudo update-alternatives --config javaws
	Enable mozilla firefox plugin:
	 ln -s /usr/lib/jvm/jdk1.7.0/jre/lib/amd64/libnpjp2.so ~/.mozilla/plugins/
-Depending on your configuration, you might need to update the apparmor profile for firefox (or other browsers) in /etc/apparmor.d/abstractions/ubuntu-browsers.d/java
-# Replace the two lines:
	/usr/lib/jvm/java-*-sun-1.*/jre/bin/java{,_vm} cx -> browser_java, 
	# /usr/lib/jvm/java-*-sun-1.*/jre/lib/*/libnp*.so cx -> browser_java,
	# with those (or adapt to your new jdk folder name)
	/usr/lib/jvm/jdk*/jre/bin/java{,_vm} cx -> browser_java,
	/usr/lib/jvm/jdk*/jre/lib/*/libnp*.so cx -> browser_java,
-Then restart apparmor:
	sudo /etc/init.d/apparmor restart