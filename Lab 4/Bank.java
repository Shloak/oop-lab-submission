import java.util.Scanner;

class Item
{
	private
		int no, credit,debit,balance;
		static int bal,acc=0;
	
	public
	 
		void getNo(int n)
		{
    			 no=n;
		}
		
		void getCredit(int cre)
		{
   			 credit=credit+cre;
		}
		
		void getDebit(int deb)
		{
   			 debit=debit+deb;
		}
		
		void getBal(int bl)
		{
   			 balance=bl;
		}
		
		int putNo()
		{
   			return no;
		}

		int putCredit()
		{ 
 			return credit;
 		}
 		
		int putDebit()
		{ 
 			return debit;
 		}
		
		int putBal()
		{ 
 			return balance;
 		}

 		Item()
		{
	 		bal=0;
	 		credit=0;
	 		debit=0;
	 		acc++;
	 		System.out.println("BALANCE IN ACCOUNT IS:"+bal);
		}
		
};//end of Item class

class Bank
{
	public static void main(String[]args)
	{
	 	int n,i,ch,ch1;
		int cr,de,num,bl;
		
		System.out.println("Enter How Many Account You Want To Create:");
 		Scanner in =new Scanner(System.in);
	 	n=in.nextInt();
	 	System.out.println("\n");
	
		Item[] array=new Item[n];	/*this statement puts size of object array to n but still we have to initiaize each 										object independently with new operator.*/

		for ( i=0; i<array.length; i++) //Creating Object
			array[i]=new Item();	
			
		System.out.println("\n");
	
		for(i=0;i<array.length;i++)		//Input Values In Object
		{
	 		System.out.println("Enter Acc. No:");
	 		num=in.nextInt();
	 		array[i].getNo(num);
 
 			do{
	 			System.out.println("\n\t01.Want To Credit");
	 			System.out.println("\n\t02.Want To Debit");
	 			System.out.println("\n\tEnter Your Choice:");
	 			ch=in.nextInt();
	 			if(ch==1)
	 			{	System.out.println("\nEnter Credit Amount:Rs.");
			 		cr=in.nextInt();
		 			array[i].getCredit(cr);
				}
				else if(ch==2)
				{	System.out.println("\nEnter Debit Amount:Rs.");
			 		de=in.nextInt();
		 			array[i].getDebit(de);
	 			}
	 			System.out.println("\n\tDo You Want To Credit Or Debit More '1'for yes & '2'for no:");
	 			ch1=in.nextInt();
	 		}while(ch1==1);
 					
 			bl=Item.bal+array[i].putCredit()-array[i].putDebit();
 			array[i].getBal(bl);
  		}
  		
  		for (i=0;i<array.length;i++)
		{	
			System.out.println("\nAccount Details:");
	  		System.out.println("Acc No:"+array[i].putNo());
	  		System.out.println("Account Balance is:Rs."+array[i].putBal());
	  	} 
		
		System.out.println("\n\nTotal Number Of Account is:"+Item.acc+"\n\n");

	}
}//end of main class
