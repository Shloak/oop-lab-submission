import java.util.Scanner;
import static java.lang.Math.pow;							//iniilizng to use power function in calclulating interest for saving account

abstract class Account									//class Account which is abstract
{	int acc, mon, year;								//data member for account number, month and year respectively
	float bal, withdraw, credit, in;						//data member for balance amount, withdraw amount, credit/deposit amount and intreaet
	String name;									//data member for storing name of the user

	Scanner input = new Scanner(System.in);
												
  public
  	
  	Account()										//Constructor
  	{	withdraw=0;									//initializing withdraw amount to 0
  		credit=0;									//initializing credit/deposit amount to 0
  		name="Null";								//initializing name of the user to NULL
		mon=0;									//initializing month to 0 
		year=0;									//initializing year to 0
		in=0;										//initializing intreset amount to 0
  	}
  	
  	abstract void with();								//Creating abstract data function withdraw
	abstract void create();								//Creating abstract data function create to create account
  	  	
  	void credit()									//Creating a member function credit to deposite amount in account
  	{	System.out.println("\nEnter the amount to Credit:");
  		credit=input.nextFloat();
  		bal+=credit;								//adding deposite money to main balance
  		System.out.println("\nYour current balance is:"+bal);
 	}
 	
 	void dis()										//Creating a member function dis to display account details

 	{	System.out.println("\nName:"+name);
 		System.out.println("Account NUmber:"+acc);
 		System.out.println("Your current balance is:"+bal);
 	}

	void intr()										//Creating a member function intr to calculate intreset amount for specific account

   	{	int m, y, t, mo;
		float f, p;
		System.out.println("\nEnter Current Month Number:");
  		m=input.nextInt();
  		System.out.println("\nEnter Current Year:");
  		y=input.nextInt();							//For calculating intreset formula used is:
		mo=(((y-year)*12)+m)-mon;						// Fianl amount=principal(1+(intreset rate/n))^n*time period
		t=mo/12;									//n= 3 for our case
		p=(float)pow((1+(0.1/3)),3*t);
		f=bal*p;
		in=f-bal;
		bal=f;
   		System.out.println("\nInterest got after "+mo+" months:"+in);
   		System.out.println("\nYour current balance is:"+bal);
   	}
 	
};
 
class Savings extends Account								//class Savings which inherits class Account
{	static int sacc=10;								//creating a static data mamber to store account number of savings account

	Scanner input=new Scanner(System.in);
	
  public
   	
   	Savings()										//Constructor
   	{	bal=1000;									//initilizing balance to 1000 as per question given
		acc=sacc;									
   	}

	void create()									//Creating a member function create to create savings account
  	{	int m, y;
		sacc++;
		acc=sacc;
  		System.out.println("\nAccount number:"+sacc);  		
  		System.out.println("\nEnter Name:");
  		name=input.nextLine();							//Entering details to account
		System.out.println("\nEnter Current Month Number:");
  		m=input.nextInt();
  		System.out.println("\nEnter Current Year:");
  		y=input.nextInt();
		mon=m;
		year=y;
  	}   	

   	void with()										//Creating a member function with to withdraw amount from savings account
  	{	System.out.println("\nEnter the amount to Withdraw:");
	  	withdraw=input.nextFloat();
  		if(withdraw <= bal-500)							//Setting the condition that amount left in account should not less than Rs.500
  		{	bal-=withdraw;							//Subtarcting the withdraw amount from the main balance of saving account
	  		System.out.println("\nYour current balance is:"+bal);
	  	}
	  	else
	  		System.out.println("\nSORRY CAN NOT WITHDRAW.Your current balance is:"+bal+" Cannot withdraw more than:"+(bal-500));
	}
	 
};

class Current extends Account								//class Current which inherits class Account
{	float overdraft;									//data member to store overdrat limit for current account
	static int cacc=20;								//creating a static data mamber to store account number of current account

	Scanner input=new Scanner(System.in);
	
  public
   	
   	Current()										//Constructor
   	{	bal=0;									//initializing balance amount to 0
		acc=cacc;
		overdraft=0;								//initializing overdraft limit amount to 0
	}

	void create()									//Creating a member function create to create current account
  	{	cacc++;
		acc=cacc;
  		System.out.println("\nAccount number:"+cacc);  		
  		System.out.println("\nEnter Name:");
  		name=input.nextLine();
		do{
			if(overdraft<500 && overdraft>10000000)			//Fixing the overdraft between Rs.500 to Rs.1 cr
				System.out.println("\nEnter overdraft limit[ranging from Rs.500/- to Rs.1 Cr]:");
			else
				System.out.println("\nEnter overdraft limit[ranging from Rs.500/- to Rs.1 Cr]:");
			overdraft=input.nextFloat();
		}while(overdraft<500 && overdraft>10000000);
  	}   	
   	   	
   	void with()										//Creating a member function with to withdraw amount from current account
  	{	System.out.println("\nEnter the amount to Withdraw:");
	  	withdraw=input.nextFloat();
		if(withdraw <= bal+overdraft)						//Setting the condition that withdraw should not be greater than balance+overdraft limit
  		{	bal-=withdraw;							//Subtarcting the withdraw amount from the main balance of current account
	  		System.out.println("\nYour current balance is:"+bal);
	  	}
	  	else
	  		System.out.println("\nSORRY CAN NOT WITHDRAW.Your current balance is:"+bal+" Cannot withdraw more than:"+(bal+overdraft));
	 }
	 
};

class Banks											//Main class
{	public static void main(String [] args)
	{	Scanner input=new Scanner(System.in);
	
		int ch,ch1, sa=0, cu=0, ac, i, j;
												//Using concept of polymorphism
		Account []sobj, cobj;							//Creating reference varialbe of class Account;creating array of reference variable
		sobj=new Savings[10];							//Creating object of class Savings using reference variable of class Account and hence using concet of polymorphism
		cobj=new Current[10];							//Creating object of class Current reference variable of class Account and hence using concet of polymorphism
				
		do{										//Perfrming do-while loop for menu
			System.out.println("\n\n1.Create Account");
			System.out.println("2.Deposit Amount");
			System.out.println("3.Withdraw Amount");
			System.out.println("4.Display Account");
			System.out.println("5.Intreset Cal On Saving Account");
			System.out.println("6.Exit");
			ch=input.nextInt();
			switch(ch)
			{	case 1:							//allows to create account
					System.out.println("\n\n1.Saving Account");
					System.out.println("2.Current Account");
					System.out.println("Enter your choice:");
					ch1=input.nextInt();
					if(ch1==1)						//allows to create a saving account
					{	sobj[sa]=new Savings();
						sobj[sa].create();
						sa++;
					}
					else if(ch1==2)					//allows to create a current account
					{	cobj[cu]=new Current();
						cobj[cu].create();
						cu++;
					}
					break;
				case 2:							//allows to deposit amount in account
					System.out.println("\nEnter Account number:");
					ac=input.nextInt();
					i=ac/10;
					if(i==1)						//allows to deposit amount in savings account
					{	j=ac%10;
						if(j>=sa)
						{	j--;
							sobj[j].credit();
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					else if(i==2)					//allows to deposit amount in current account
					{	j=ac%10;
						if(j>=cu)
						{	j--;
							cobj[j].credit();
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					break;
				case 3:							//allows to withdraw amount from account
					System.out.println("\nEnter Account number:");
					ac=input.nextInt();
					i=ac/10;
					if(i==1)						//allows to withdraw amount from savings account
					{	j=ac%10;
						if(j>=sa)
						{	j--;
							sobj[j].with();
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					else if(i==2)					//allows to withdraw amount from current account
					{	j=ac%10;
						if(j>=cu)
						{	j--;
							cobj[j].with();
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					break;
				case 4:							//allows to display details of account
					System.out.println("\nEnter Account number:");
					ac=input.nextInt();
					i=ac/10;
					if(i==1)						//allows to display details of savings account
					{	j=ac%10;
						if(j>=sa)
						{	j--;
							sobj[j].dis();
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					else if(i==2)					//allows to display details of current account
					{	j=ac%10;
						if(j>=cu)
						{	j--;
							cobj[j].dis();
						}
						else
							System.out.println("\nNo Such Account Number Exist...");
					}
					break;
				case 5:							//allows to calculate interest for saving account
					System.out.println("\nEnter Account number:");
					ac=input.nextInt();
					i=ac/10;
					if(i==1)
					{	j=ac%10;
						j--;
						sobj[j].intr();
					}
					else if(i==2)
						System.out.println("\nNo Such Account Number Exist...");
			}
		}while(ch<6);
	}
}//END OF PROGRAMM
/*REFRENCE
FROM FRIENDS
1401081	Kedar Vishwajit Acharya
1401070	Nisarg Satish Tike
*/