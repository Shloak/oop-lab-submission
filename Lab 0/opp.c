//***************************************************************
//    	HEADER FILES
//****************************************************************

#include<stdio.h>
#include<conio.h>
#include <stdlib.h>

//***************************************************************
//    	STRUCTURE FOR CUSTOMER
//****************************************************************

struct cus
{   long int no;
    char name[50];
}c[6]={    {1234567890,"SUDHIR GOEL"},         //INITIALISING CUSTOMER DETAILS
           {1234567891,"RAVI PATEL"},
           {1234567892,"JEET PAREKH"},
           {1234567893,"SHARAD RATHI"},
           {1234567894,"BITTU THAKER"},
           {1234567895,"MANAN GUPTA"}   };

//***************************************************************
//    	STRUCTURE FOR PRODUCT
//****************************************************************

struct pro
{   int prono;
    char proname[20];
    int price;
}product[6]={   {1,"KEYBOARD",200},         //INITIALISING PRODUCT DETAILS
                {2,"MOUSE(T-Link)",100},
                {3,"MONITOR",900},
                {4,"RAM(2 GB)",300},
                {5,"PEN DRIVE",500},
                {6,"HARD DISK",1000}   };

//***************************************************************
//    	FUNCTION FOR CREATING A NEW CUSTOMER ACCOUNT
//****************************************************************

void show_cus()
{   system("cls");
    int i;
    printf("\n\n--------------------------------------------------------------------------------");
	printf("\n\n Sr No.\t Customer Name\t\t Customer Phone No.\n");
	printf("\n--------------------------------------------------------------------------------");
	for(i=0;i<6;i++)
        printf("\n %d \t %s \t\t %ld", (i+1), c[i].name, c[i].no);
    printf("\n\n\t\tenter to continue");
    getch();
}

//***************************************************************
//    	FUNCTION FOR SEARCHING A CUSTOMER ACCOUNT
//****************************************************************

void cus_search(int a)
{   system("cls");
    printf("\n\t\tCUSTOMER INFO\n");
    printf("\n\tCUSTOMER NAME: ");
    puts(c[a].name);
    printf("\n\tCUSTOMER PHONE NUMBER: %ld\n", c[a].no);
    printf("\n\n\t\tenter to continue");
    getch();
}

//***************************************************************
//    	FUNCTION OF CUSTOMER DETAIL
//****************************************************************

void cus_detail()
{   int i,flag=0;
    long int ph;
    char ch;
    do
	{   system("cls");
		printf("\n\n\n\tMENU");
		printf("\n\n\t1. SHOW ALL CUSTOMER");
		printf("\n\n\t2. SEARCH CUSTOMER");
		printf("\n\n\t3. RETURN TO MAIN MENU");
		printf("\n\n\tPlease Select Your Option (1-3) ");
		scanf("%c", &ch);
		switch(ch)
		{
			case '1':
                show_cus();
				break;
			case '2':
			    system("cls");
			    printf("\n\tEnter Customer Phone Number: ");
                scanf("%ld", &ph);
                for(i=0;i<6;i++)
                    if(c[i].no==ph)
                        {   cus_search(i);
                            flag =1;
                        }
                if(!flag)
                {    printf("\n\tSORRY NO SUCH CUSTOMER EXIST");
                     getch();
                }
				break;
            case '3':
                main_menu();
			default:
			    cus_detail();
		}
	}while(ch!='3');

}

//***************************************************************
//    	FUNCTION FOR ADDING A NEW PRODUCT
//****************************************************************

void show_pro()
{   system("cls");
    int i;
    printf("\n\n--------------------------------------------------------------------------------");
	printf("\n\n Sr No.\t Pr No.\t Pr Name\t\tPrice\n");
	printf("\n--------------------------------------------------------------------------------");
	for(i=0;i<6;i++)
        printf("\n %d \t %d \t %s \t\t %d", (i+1), product[i].prono, product[i].proname, product[i].price);
    printf("\n\n\t\tenter to continue");
    getch();
}

//***************************************************************
//    	FUNCTION FOR SEARCHING A CUSTOMER ACCOUNT
//****************************************************************

void pro_search(int a)
{   system("cls");
    printf("\n\t\tPRODUCT INFO\n");
    printf("\n\tPRODUCT NUMBER: %d\n", product[a].prono);
    printf("\n\tPRODUCT NAME: ");
    puts(product[a].proname);
    printf("\n\tPRODUCT PRICE: %d\n", product[a].price);
    printf("\n\n\t\tenter to continue");
    getch();
}

//***************************************************************
//    	FUNCTION OF PRODUCT DETAIL
//****************************************************************

void pro_detail()
{   int pno,i,flag=0;
    char ch;
    system("cls");
    do
	{   printf("\n\n\n\tMENU");
		printf("\n\n\t1. SHOW ALL PRODUCT");
		printf("\n\n\t2. SEARCH PRODUCT");
		printf("\n\n\t3. RETURN TO MAIN MENU");
		printf("\n\n\tPlease Select Your Option (1-3) ");
		scanf("%c", &ch);
		switch(ch)
		{
			case '1':
                show_pro();
				break;
			case '2':
			    system("cls");
			    printf("\n\tEnter Product Number: ");
                scanf("%d", &pno);
                for(i=0;i<6;i++)
                    if(product[i].prono==pno)
                        {   pro_search(i);
                            flag =1;
                        }
                if(!flag)
                {    printf("\n\tSORRY NO SUCH PRODUCT EXIST");
                     getch();
                }
				break;
            case '3':
                main_menu();
			default:
			    pro_detail();
		}
	}while(ch!='3');
}

//***************************************************************
//    	INTRODUCTION FUNCTION
//****************************************************************

void intro()
{
    system("cls");
	printf("\n\t\t\t   GLORIOUS ELECTRONICS\n");
	printf("\n\n\t\t         MADE BY : SHLOAK AGARWAL");
	printf("\n\n\t\t             ROLL NO : 1401105");
	printf("\n\n\t\t             enter to continue");
	getch();
}

//***************************************************************
//  FUNCTION TO PLACE ORDER AND GENERATING BILL FOR PRODUCTS
//****************************************************************

void bill()
{   system("cls");
    int  total=0,qty=0,amt,order_arr[50],quan[50],c=0,i,x;
	char ch;
	show_pro();
	printf("\n................................................................................");
	printf("\n\t\t\t\tPLACE YOUR ORDER");
	printf("\n................................................................................\n");
	do{     printf("\n\nEnter The Product No. Of The Product : ");
            scanf("%d",&order_arr[c]);
            printf("\nQuantity in number : ");
            scanf("%d", &quan[c]);
            c++;
            printf("\nDo You Want To Order Another Product ? (y/n)");
            ch = getche();
            printf("\n");
	}while( ch == 'y' || ch == 'Y' );
	printf("\n\nThank You For Placing The Order");
	getch();
	system("cls");
	printf("\n\t\t\t  GLORIOUS ELECTRONICS");
	printf("\n\t\t\t\tCASH MEMO");
	printf("\n\n--------------------------------------------------------------------------------");
	printf("\n\n Sr No.\t Pr No.\t Pr Name\t\tQty \tPrice \t Amount\n");
	printf("\n--------------------------------------------------------------------------------");
	for(x=0;x<=c;x++)
	{
	    for(i=0;i<6;i++)
        {   if(product[i].prono==order_arr[x])
            {
				amt=(product[i].price)*(quan[x]);
				printf("\n %d \t %d \t %s \t\t %d \t %d \t %d", (x+1), order_arr[x], product[i].proname, quan[x], product[i].price, amt);
				total+=amt;
				qty+=quan[x];
			 }

		}
	}
	printf("\n\n--------------------------------------------------------------------------------");
	printf("\t\tQUANTITY = %d \t\tTOTAL = %d", qty, total);
	printf("\n\n--------------------------------------------------------------------------------");
}

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

void main_menu()
{   char ch;
    do
	{   system("cls");
        printf("\n\n\n\tMAIN MENU");
		printf("\n\n\t1. CUSTOMER DETAILS");
		printf("\n\n\t2. PRODUCT DETAILS");
		printf("\n\n\t3. GENERATE BILL");
        printf("\n\n\t4. EXIT");
		printf("\n\n\tPlease Select Your Option (1-4) ");
		scanf("%c",&ch);
		switch(ch)
		{
			case '1':
				cus_detail();
				main_menu();
			case '2':
			    pro_detail();
				main_menu();
			case '3':
			    bill();
            case '4':
                exit(0);
		}
	}while(ch!='4');
}

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

int main()
{
 	intro();
	main_menu();

	return 0;
}

//***************************************************************
//    			END OF PROGRAM
//***************************************************************
//***************************************************************
//    			Refernce
//***************************************************************
/*
http://stackoverflow.com/questions/17793059/clrscr-equivalent-in-codeblocks
Programming in ANSIC by E Balagurusamy Fifth Edition
Computer Science with C++ Class XII by Sumita Arora
Computer Science with C++ Class XI by Sumita Arora
*/
//***************************************************************

