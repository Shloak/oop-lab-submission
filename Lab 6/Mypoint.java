import java.util.Scanner;

class Point
{	int []pt;
	int n;						//for taking size of dimension

	Scanner input=new Scanner(System.in);
	
   public
	
	Point(int a)					//Regular Constructor
	{	n=a;
		pt=new int[n];
	}

	Point(Point p)					//Copy Constructor
	{	n=p.n;
		pt=new int[n];
		for(int i=0;i<n;i++)
			pt[i]=p.pt[i];
	}
	
	void add()						//Function for taking points from user
	{	for(int i=0;i<n;i++)
		{	System.out.println("Enter Dimensions Of The Point:");
			pt[i]=input.nextInt();
			System.out.println(" ");
		}
	}
	
	void check(Point p)				//Function for checking points equalality
	{	System.out.println(" ");
		int a=0;
		if(n==p.n)
		{	for(int i=0;i<n;i++)
				if(pt[i]==p.pt[i])
					a++;
		if(a==n)
			System.out.println("Points Are Equal");
		}
		else
			System.out.println("Points Are Not Equal");
		System.out.println(" ");
	}

	void show()						//Function for Displaying the point
	{	System.out.println(" ");
		System.out.println("Dimension Of The Point Is:"+n);
		for(int i=0;i<n;i++)
			System.out.print(" "+pt[i]); 
		System.out.println(" ");
	}

}

class Mypoint
{	public static void main(String agrs[])
	{	Scanner input=new Scanner(System.in);
		
		int n,m;
		
		System.out.println("Enter The Number Of Dimensions Of The Point1:");
		n=input.nextInt();
		System.out.println(" ");

		Point p1=new Point(n);			//Calling constructor

		p1.add();
		
		Point p2=new Point(p1);			//Calling Copy constructor

		System.out.println("Checking Equality Betweem Point1 And Its Copy");
		p1.check(p2);				//Checking point of object p1 and p2

		System.out.println("Enter The Number Of Dimensions Of The Point2:");
		m=input.nextInt();
		System.out.println(" ");

		Point p3=new Point(m);			

		p3.add();					//Creating a object p3
		
		System.out.println("Checking Equality Betweem Point1 And Point2");
		p1.check(p3);				//Checking point of object p1 and p3

		System.out.println("Displaying Point1");
		p1.show();

		System.out.println("Displaying Point2");
		p3.show();
	}
}