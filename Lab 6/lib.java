import java.util.Scanner;

class Media
{	String title;										//REQUIRES PASSWORD WHICH IS 1810	
	int price, year;
 	
 	Scanner input = new Scanner(System.in);
 
  public
 	
 	Media()												//constructor
 	{	title=new String("NULL");
 		price=0;
 		year=0;
 	} 	
 	
 	void settitle()										//for setting title
 	{	System.out.print("Enter Title:");
 		title=input.nextLine();
 	}
 	
 	String gettitle()									
 	{	return title;	}
 	
 	void setprice()										//for setting price
 	{	System.out.print("Enter Price:");	
 		price=input.nextInt();
 	}
 	
 	int getprice()
 	{	return price; }
 	
 	void setyear()										//for setting year
 	{	System.out.print("Enter Year:");	
 		year=input.nextInt();
 	}
 	
 	int getyear()
 	{ 	return year;	}
 	
 }
 
 class Book extends Media								//class book inherit class Media
 {	String author;
 	int pg;
 	
 	Scanner input = new Scanner(System.in);
 	
   public
   	
   	Book()												//constructor
   	{	author=new String("NULL");
   		pg=0;
   	}
   	
   	void setauthor()									//for setting author
 	{	System.out.print("Enter Author:");
 		author=input.nextLine();
 	}
 	
 	String getauthor()								
 	{	return author;	}
 	
 	void setpg()										//for setting page number
 	{	System.out.print("Enter Page Number:");	
 		pg=input.nextInt();
 	}
 	
 	int getpg()				
 	{	return pg; }
 	
 }
 
 class CD extends Media									//class cd inherit class Media
 {	float size, time;
 
 	Scanner input = new Scanner(System.in);
 
   public
   	 
   	CD()												//constructor
   	{	size=0;
   		time=0;
   	}
   	
   	void setsize()										//for setting size of cd
 	{	System.out.print("Enter Size Of CD(in MB):");	
 		size=input.nextFloat();
 	}
 	
 	float getsize()
 	{	return size; }
 	
 	void settime()										//for setting duration of cd
 	{	System.out.print("Enter Duration(in min):");	
 		time=input.nextFloat();
 	}
 	
 	float gettime()
 	{ 	return time;	}
 	
 }
 
 class lib//main prog
 {	public static void main(String args[])
	{	Scanner input = new Scanner(System.in);
		
		int i,ch;
		
		Book []b=new Book[3];							//array of object of class book
		for(i=0;i<3;i++)
			b[i]=new Book();
			
		CD []c=new CD[3];								//array of object of class book
		for(i=0;i<3;i++)
			c[i]=new CD();
	
		do{
	 			System.out.println("\n\t1.Set Info Of Book");
				System.out.println("\n\t2.Set Info Of CD");
				System.out.println("\n\t3.Get Info Of Book");
				System.out.println("\n\t4.Get Info Of CD");
				System.out.println("\n\t5.Exit");
	 			System.out.println("\n\tEnter Your Choice:");
	 			ch=input.nextInt();
	 			switch(ch)
				{
					case 1:			//Setting info of book
						int password;
						System.out.print("\nEnter Password:");
						password=input.nextInt();
						if(password==1810)				//REQUIRES PASSWORD WHICH IS 1810	
						{	for(i=0;i<2;i++)
							{	System.out.print("\nEnter Book"+(i+1)+"\n");
								b[i].settitle();
								b[i].setprice();
								b[i].setyear();
								b[i].setauthor();
								b[i].setpg();
								System.out.println();
							}
						}
						else
						System.out.print("\nPassword Wrong");						
						break;
					case 2:			//Setting info of cd
						int pass1;
						System.out.print("\nEnter Password:");
						pass1=input.nextInt();
						if(1810==pass1)					//REQUIRES PASSWORD WHICH IS 1810	
						{	for(i=0;i<2;i++)
							{	System.out.print("\nEnter CD"+(i+1)+"\n");
								c[i].settitle();
								c[i].setprice();
								c[i].setyear();
								c[i].setsize();
								c[i].settime();
								System.out.println();
							}
						}
						else
						System.out.print("\nPassword Wrong");						
						break;
					case 3:			//Getting info of book
						for(i=0;i<2;i++)
						{	System.out.print("\nBook"+(i+1));
							System.out.print("\nTitle:"+b[i].gettitle());
							System.out.print("\nPrice:"+b[i].getprice());
							System.out.print("\nYear:"+b[i].getyear());
							System.out.print("\nAuthor:"+b[i].getauthor());
							System.out.print("\nPage Number:"+b[i].getpg());						
							System.out.println();
						}
						break;
					case 4:			//Getting info of CD
						for(i=0;i<2;i++)
						{	System.out.print("\nCD"+(i+1));
							System.out.print("\nTitle:"+c[i].gettitle());
							System.out.print("\nPrice:"+c[i].getprice());
							System.out.print("\nYear:"+c[i].getyear());
							System.out.print("\nSize Of CD(in MB):"+c[i].getsize());
							System.out.print("\nDuration(in min):"+c[i].gettime());
							System.out.println();							
						}
						break;
					
				}

	 		}while(ch<5);
	}
}
//end of prog
