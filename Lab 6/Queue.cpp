//***************************************************************
//    	HEADER FILES
//****************************************************************                       Run in Code::Blocks or Dec-C++

#include<iostream>
#include<stdlib.h>
#include<conio.h>
																						//First in first out
					      																		  
using namespace std;

//***************************************************************
//    	CLASS FOR QUEUE
//****************************************************************

class Queue
{
	private:
		int *qu;														// Pointer to int
		int front,rear,n;												//n for size of stack
		
	public:
		Queue();														//Constructors...
		
		void size();													//Member Functions...	
		friend Queue operator +(Queue Q ,int x);
		Queue operator --(int x);
		friend Queue operator <<(Queue Q, int x);

};

Queue::Queue()															//Constructors...
{
    qu = '\0';
    n = 0;
    front = -1;
    rear = -1;
}

//***************************************************************
//    	FUNCTION FOR SIZE NEEDED FOR QUEUE	
//****************************************************************

void Queue::size()
{	cout<<"\n\n\tEnter Size Of Queue:";
	cin>>n;
	qu = new int[n];														// Allocate n ints and save ptr in a
}
					      																		  
//***************************************************************
//    	FUNCTION FOR PUSHING A ELEMENT IN THE QUEUE
//****************************************************************

Queue operator +(Queue Q ,int x)											//Function for adding number in queue
{	if((Q.rear-Q.front)==Q.n-1)
		cout<<"\n\n\t\t overflow";
	else
	{	Q.rear++;
		Q.qu[Q.rear]=x;
		cout<<"\n\n\t\t element pushed:";
		if(Q.front==-1)
			Q.front=0;
	}
	getch();
	return Q;
}

//***************************************************************
//    	FUNCTION FOR POPING A ELEMENT FROM THE QUEUE
//****************************************************************

Queue Queue::operator --(int x)												//Function for deleting a number from queue
{   if(front==-1)
		cout<<"\n\n\t\t underflow:";
	else
	{	int i=qu[front];
		if(front==rear)
		{	front=-1;
			rear=-1;
		}
		else
			front++;
		cout<<"\n\n\t\t element poped:"<<i;
		
	}
	getch();
	return *this;
}

//***************************************************************
//    	FUNCTION FOR DISPLAYING THE ELEMENTS FROM THE QUEUE
//****************************************************************

Queue operator<<(Queue Q, int x)											//Function for showing numbers in the queue
{   if(Q.front==-1)
		cout<<"\n\n\t\t Empty:";
	else
	{	cout<<"\n\n\t";
		for(int i=Q.front;i<= Q.rear ;i++)
			cout<<" "<<Q.qu[i];
		cout<<"\n\n\t\t Front= "<<Q.front;
		cout<<"\n\t\t Rear= "<<Q.rear;
	}
	getch();
	return Q;
}

//***************************************************************
//    	INTRODUCTION FUNCTION
//****************************************************************

void intro()
{
    system("cls");
    cout<<"\n\n\t\t         MADE BY : SHLOAK AGARWAL";
    cout<<"\n\n\t\t             ROLL NO : 1401105";
    cout<<"\n\n\t\t             enter to continue";
    getch();
}

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

int main()
{	intro();
	Queue q;
	int ch;
	system("cls");
	q.size();
    do
	{   system("cls");
		cout<<"\n\n\tMAIN MENU";
		cout<<"\n\n\t 1. Insert";
		cout<<"\n\n\t 2. Delet";
		cout<<"\n\n\t 3. Display";
		cout<<"\n\n\t 4. Exit";
		cout<<"\n\n\tPlease Select Your Option (1-4) ";
		cin>>ch;
		switch(ch)
		{	case 1:		//Adding number in queue
				int x;
				cout<<"\n Enter element:";
				cin>>x;
				q=q+x;
				break;
			case 2:		//Deleting number from queue
				q--;
				break;
			case 3:		//Displaying number from queue
				q=q<<0;
				break;
		}
	}while(ch<4);
	return 0;
}

//***************************************************************
//    			END OF PROGRAM
//***************************************************************

