import java.util.Scanner;


class Mat
{	int[][] array;
	Scanner in =new Scanner(System.in); 
	int row, col;

    public
		 
	Mat(int m, int n)						//REGULAR CONSTRUCTOR
	{	row=m;
		col=n;
		array=new int[row][col];
		for(int i=0; i<row;i++)
			for(int j=0;j<col;j++)
				array[i][j]=0;
	}

	void assg(int m,int n)					//Enter Value in Array
	{	int a;
		System.out.println("\nEnter Matrix");
		for(int i=0; i<row;i++)
			for(int j=0;j<col;j++)
			{	a=in.nextInt();
				array[i][j]=a;
			}
	}

	Mat(Mat m)							//COPY CONSTRUCTOR
	{	row=m.row;
		col=m.col;
		array=new int[row][col];
		for(int i=0; i<row;i++)
			for(int j=0;j<col;j++)
				array[i][j]=m.array[i][j];
	}

	void ext()							//Extract
	{	int a;	
		System.out.println("\nOutput Matrix");
		for(int i=0; i<row;i++)
		{	System.out.println("\n");
			for(int j=0;j<col;j++)
			{	a=array[i][j];
				System.out.print(" "+a);
			}
		}
	}

	void trans()						//Transpose
	{	System.out.println("\nTranspose Of Matrix");
		for(int i=0; i<row;i++)
		{	System.out.println("\n");
			for(int j=0;j<col;j++)
			{	int a=0;
				a=array[j][i];
				System.out.print(" "+a);
			}
		}
	}

	void sum(Mat m)						//Sum
	{	if((row==m.row)&&(col==m.col))
		{	System.out.println("\nMatrices Can Be Added\n");	
			System.out.println("\nSum Of Matrices");
			for(int i=0; i<row;i++)
			{	System.out.println("\n");
				for(int j=0;j<col;j++)
				{	int a=0;
					a=array[i][j]+m.array[i][j];
					System.out.print(" "+a);
				}
			}
		}
		else 
			System.out.println("\nMatrices Cannot be Added");
	}

	void mult(Mat m)						//Multiplication
	{	if(col==m.row)
		{	System.out.println("\nMatrices Can Be Multiplied\n");	
			System.out.println("\nMultiplication Of Matrices");
			for(int i=0; i<row;i++)
			{	System.out.println("\n");
				for(int j=0;j<m.col;j++)
				{	int a=0;
					for(int k=0;k<col;k++)
						a=a+(array[i][k]*m.array[k][j]);
					System.out.print(" "+a);
				}
			}
		}
		else 
			System.out.println("\nMatrices Cannot be Multiplied");
	}

	void sca()							//Scalar
	{	System.out.println("\nEnter A Number");
		int ch1;
		ch1=in.nextInt();
		for(int i=0; i<row;i++)
		{	System.out.println("\n");
			for(int j=0;j<col;j++)
			{	int a;
				a=ch1*array[i][j];
				System.out.print(" "+a);
			}
		}
	}
	
}

class Mymat
{	public static void main(String[]args)
	{	Scanner in =new Scanner(System.in); 
		int m,n,ch;		
		
		System.out.println("\nAssign values to a matrix");
		System.out.println("\nInput Row");
 
		m=in.nextInt();
		System.out.println("\nInput Column");
		n=in.nextInt();
		Mat m1=new Mat(m,n);
		m1.assg(m,n);					//CALLING REGULER CONSTRUCTOR

		Mat m2=new Mat(m1);				//CALLING COPY CONSTRUCTOR
	
		do{
	 			System.out.println("\n\t1.Extract values from a matrix");
				System.out.println("\n\t2.Find transpose of a matrix");
				System.out.println("\n\t3.Add two matrices");
				System.out.println("\n\t4.Multiply two matrices");
				System.out.println("\n\t5.Multiply a matrix with a scalar value");
				System.out.println("\n\t6.Exit");
	 			System.out.println("\n\tEnter Your Choice:");
	 			ch=in.nextInt();
	 			switch(ch)
				{
					case 1:			//Extract
						m1.ext();
						break;
					case 2:			//Transpose
						m1.trans();
						break;
					case 3:			//Sum
						m1.sum(m2);
						break;
					case 4:			//Multiplication
						m1.mult(m2);
						break;
					case 5:			//Scalar
						m1.sca();
						break;
					
				}

	 		}while(ch<6);
	}
}
//end of program
