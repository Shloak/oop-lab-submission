//***************************************************************
//    	HEADER FILES
//****************************************************************                       Run in Code::Blocks or Dec-C++

#include<iostream>
#include<stdlib.h>
#include<conio.h>
					      																		  
using namespace std;

//***************************************************************
//					CLASS PLOYNOMIAL
//****************************************************************

class Polynomial
{
	int *array1;													//For co-efficient of the terms			Pointer to int
	int *array2;													//For degree of the terms
	static int n;
	static int a;
	
  public:
   	
   	Polynomial(int x)												//Constructor With Parameters
	{	n=x;
	 	a=x;
		array1 = new int[a+1];
		array2 = new int[a+1];
	}

	Polynomial()													//Constructor Without Parameters		
	{ }									
   										
   	~Polynomial()													//Destructor
	{	delete array1;
		delete array2;
	}
   	
   	friend ostream &operator<<( ostream &output, const Polynomial &P )		//friend func to overload extraction operator
    { 	
    	cout<<"\n Polynomial:";
        for(int i=0;i< a+1 ;i++)
        {
         	output<<"(";	
			output << P.array1[i] ;
			output<<"x^";
			output << P.array2[i] ;
			output<<")";
         	if(i< a )
        		output << "+";
        }
        return output;
    }
     
    friend istream &operator>>( istream  &input, Polynomial &P )		//friend func to overload insertion operator
    { 
        cout<<"\n Enter Coff:";
		for(int i=0;i<n+1;i++)
        	input >> P.array1[i];
		cout<<"\n\n Enter Power dec Order";
		for(int i=0;i<n+1;i++)
        	input >> P.array1[i];
        return input;            
	}

	Polynomial operator +(Polynomial &p)							//Function to overload + operator
	{	Polynomial re;
		for(int i=0;i<a+1;i++)
			if(p.array2[i]==array2[i])
				re.array1[i]=p.array1[i]+array1[i];
		return re;
	}
	
	Polynomial operator -(Polynomial &p)								//Function to overload - operator
	{	Polynomial re;
		for(int i=0;i<n+1;i++)
			if(p.array2[i]==array2[i])
				re.array1[i]=array1[i]-p.array1[i];
		return re;
	}
	
	Polynomial operator *(Polynomial &p)								//Function to overload * operator
	{	Polynomial re;
		int k=0;
		for(int i=0;i<n+1;i++)
			for(int j=0;j<n;j++)
			{	re.array1[k]=p.array1[i]*array2[j];
				re.array2[k]=p.array2[i]+array2[j];
				k++;
			}
		return re;
	}
	   	
};//end of class Polynomial

//***************************************************************
//    	THE MAIN FUNCTION OF PROGRAM
//****************************************************************

int main()
{
	int ch,n,ch1;
	cout<<"\n Enter The Highest Degree Of The Polynomial:";
	cin>>n;
	
	Polynomial poly(n);											//Creating an object
	
	cin>>poly;													//Using insertion-to set value
	
	Polynomial poly2=poly;										//Copying a object poly to copy2 using copy constructor
	
	Polynomial poly3(n);										//Creating an object
		
    do
	{   system("cls");
		cout<<"\n\n\tMAIN MENU";
		cout<<"\n\n\t 1. Add";
		cout<<"\n\n\t 2. Subtract";
		cout<<"\n\n\t 3. Multiply";
		cout<<"\n\n\t 4. Display";
		cout<<"\n\n\t 5. Exit";
		cout<<"\n\n\tPlease Select Your Option (1-5) ";
		cin>>ch;
		switch(ch)
		{	case 1://Add
				poly3=poly+poly2;
				break;
			case 2://Subtarct
				poly3=poly-poly2;
				break;
			case 3://Multiply
				poly3=poly*poly2;
				break;
			case 4://Display
				cout<<"\n For Original Equn press 1 else 2:";
				cin>>ch1;
				if(ch1==1)
					cout<<poly;
				else
					cout<<poly3;
				break;
			case 5://Exit
				exit(0);
		}
	}while(ch!='5');
	return 0;
}

//End of programm
